#include <iostream>
#include <exception>
#include <chrono>

#include <omp.h>

#include "diag/Timer.h"

#include "impl/InputData.h"
#include "impl/LabelData.h"
#include "impl/Network.h"


void initOpenMP() {
    int maxThreads = omp_get_max_threads();
    if (maxThreads > 16) {
        omp_set_num_threads(16);
    }
}

void minstProjectSolution() {
    auto trainTime = std::chrono::minutes(29);

    // Load and preprocess training vector
    auto trainingPixels = InputData<28 * 28>::loadInputs("data/fashion_mnist_train_vectors.csv", 255.0f, 60000);

    // Load and preprocess training labels
    auto trainingLabels = LabelData<10>::loadLabels("data/fashion_mnist_train_labels.csv", 60000);

    // Create the network
    Network<28 * 28, 10> network({ 40 });

    // Train the network with training vector
    network.train(trainingPixels, trainingLabels, trainTime,
        0.031f, /* stepSize */
        0.63f,  /* momentum factor */
        100,    /* batch size */
        1000,   /* validation set size */
        0.03f,  /* step size decay rate */
        0.0f,   /* noise addition */
        0.0005f /* weight decay rate */
    );

    // Evaluate the training vector on the final network and print out the results
    NetworkResults trainPredictions = network.evaluate(trainingPixels);
    trainPredictions.printOut("trainPredictions");

    // Load and preprocess test vector
    auto testPixels = InputData<28 * 28>::loadInputs("data/fashion_mnist_test_vectors.csv", 255.0f, 10000);

    // Evaluate the test vector on the final network and print out the results
    NetworkResults testPredictions = network.evaluate(testPixels);
    testPredictions.printOut("actualTestPredictions");
}

void digitsProjectSolution() {
    auto trainTime = std::chrono::minutes(1);

    // Load and preprocess training vector
    auto trainingPixels = InputData<28 * 28>::loadInputs("data/digits_train_vectors.csv", 255.0f, 60000);

    // Load and preprocess training labels
    auto trainingLabels = LabelData<10>::loadLabels("data/digits_train_labels.csv", 60000);

    // Create the network
    Network<28 * 28, 10> network({ 40 });

    // Train the network with training vector
    network.train(trainingPixels, trainingLabels, trainTime,
        0.031f, /* stepSize */
        0.63f,  /* momentum factor */
        100,    /* batch size */
        10000,   /* validation set size */
        0.03f,  /* step size decay rate */
        0.0f,   /* noise addition */
        0.0005f /* weight decay rate */
    );

    // Evaluate the training vector on the final network and print out the results
    NetworkResults trainPredictions = network.evaluate(trainingPixels);
    trainPredictions.printOut("trainPredictions");

    // Load and preprocess test vector
    auto testPixels = InputData<28 * 28>::loadInputs("data/digits_test_vectors.csv", 255.0f, 10000);

    // Evaluate the test vector on the final network and print out the results
    NetworkResults testPredictions = network.evaluate(testPixels);
    testPredictions.printOut("actualTestPredictions");
}

void xorSolution() {
    auto trainTime = std::chrono::seconds(10);

    // Load and preprocess training vector
    auto trainingPixels = InputData<2>::loadInputs("data/xor_train_vectors.csv", 50.0f, 60000);

    // Load and preprocess training labels
    auto trainingLabels = LabelData<2>::loadLabels("data/xor_train_labels.csv", 60000);

    // Create the network
    Network<2, 2> network({ 16, 16, 16 });

    // Train the network with training vector
    network.train(trainingPixels, trainingLabels, trainTime,
        0.04f,  /* stepSize */
        0.95f,  /* momentum factor */
        1000,   /* batch size */
        0,      /* validation set size */
        0.0f,   /* step size decay rate */
        0.0f,   /* noise addition */
        0.0f    /* weight decay rate */
    );

    // Evaluate the training vector on the final network and print out the results
    NetworkResults trainPredictions = network.evaluate(trainingPixels);
    trainPredictions.printOut("trainPredictions");

    // Load and preprocess test vector
    auto testPixels = InputData<2>::loadInputs("data/xor_test_vectors.csv", 50.0f, 10000);

    // Evaluate the test vector on the final network and print out the results
    NetworkResults testPredictions = network.evaluate(testPixels);
    testPredictions.printOut("actualTestPredictions");
}

int main(void) {
    Timer t("mainTimer");

    initOpenMP();

    try {
        minstProjectSolution();
        //digitsProjectSolution();
        //xorSolution();
    } catch (const std::exception& e) {
        std::cerr << e.what() << std::endl;
    }

    return 0;
}
