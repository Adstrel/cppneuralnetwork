from matplotlib import pyplot as plt
import numpy as np


# Original data/image
CSV = np.genfromtxt('digits_test_vectors.csv', delimiter=',')
plt.tight_layout()

for a in range(100):
    IMG = CSV[a].reshape((28, 28))

    plt.imshow(IMG)

    plt.savefig(f"x/mygraph{a}.png")
