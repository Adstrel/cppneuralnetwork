#pragma once

#include <vector>
#include <cstdlib>


/**
* Matrix class that stores data in one array but can be accessed with double brakets -- mat[row][col]
*
* Extra overhead is optimized away
*/
template<typename T>
struct Matrix {
    std::vector<T> data;
    size_t rows;
    size_t cols;

    Matrix()
        : data(0), rows(0), cols(0) {

    }

    Matrix(size_t _rows, size_t _cols)
        : data(_rows * _cols), rows(_rows), cols(_cols) {
    }

    void resetRandom() {
        for (size_t i = 0; i < data.size(); i++) {
            data[i] = static_cast<T>(0.5) - static_cast<T>(rand()) / static_cast<T>(RAND_MAX);
        }
    }

    void resetZero() {
        memset(data.data(), 0, sizeof(T) * data.size());
    }

    inline T* operator[](size_t i) {
        return data.data() + cols * i;
    }

    inline const T* operator[](size_t i) const {
        return data.data() + cols * i;
    }
};
