#pragma once

#include <iostream>
#include <algorithm>


class BatchSelector {
    
    //PointerArray = Array of randomly sorted indexes of (entry, output), given by batch size and having size of <0...trainSize).
    //<TrainSie...pointerArray.size) hold indexes of testing data for early-stopping.
    std::vector<size_t> allPointerArray;
    std::vector<size_t> currentBatch;
    std::vector<size_t> validationSet;

    size_t batchSize;
    size_t batchIndex;
    size_t trainSize;

public:
    BatchSelector(size_t dataSize, size_t oneBatchSize, size_t testDataSize) {
        batchSize = oneBatchSize;
        trainSize = dataSize - testDataSize;
        batchIndex = 0;

        for (size_t i = 0; i < dataSize; i++) {
            allPointerArray.push_back(i);
        }
        std::random_shuffle(allPointerArray.begin(), allPointerArray.end());

        currentBatch.reserve(oneBatchSize);
        validationSet = std::vector<size_t>(allPointerArray.begin() + trainSize, allPointerArray.begin() + allPointerArray.size());
    }

    void goToStart() {
        batchIndex = 0;
    }

    bool hasNext() const {
        return batchIndex < trainSize;
    }


    const std::vector<size_t>& getNextBatch() {
        size_t endIndex = std::min(trainSize, batchIndex + batchSize);

        currentBatch.resize(endIndex - batchIndex);
        for (size_t i = 0; i < currentBatch.size(); i++) {
            currentBatch[i] = allPointerArray[i + batchIndex];
        }

        batchIndex = endIndex;
        return currentBatch;
    }

    const std::vector<size_t>& getTestSet() const {
        return validationSet;
    }
    
    size_t getTrainingSize() const {
        return trainSize;
    }

    const std::vector<size_t>& getTrainingSet() const {
        return allPointerArray;
    }
};
