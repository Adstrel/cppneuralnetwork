import os, random


def generate(vectors, labels, N, maxx):
    with open(vectors, 'w') as f_vectors:
        with open(labels, 'w') as f_labels:
            for i in range(N):
                x, y = None, None
                while (not x or not y):
                    x, y = random.randint(0, maxx) - maxx // 2, random.randint(0, maxx) - maxx // 2
                result = int(bool(x > 0) ^ bool(y > 0))

                f_vectors.write(str(x) + ',' + str(y) + '\n')
                f_labels.write(str(result) + '\n')

def main():
    generate('xor_train_vectors.csv', 'xor_train_labels.csv', N=60000, maxx=100)
    generate('xor_test_vectors.csv',  'xor_test_labels.csv',  N=10000, maxx=100)


if __name__ == '__main__':
    random.seed(0)
    main()
