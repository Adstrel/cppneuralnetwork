#pragma once

#include <vector>
#include <string>
#include <iostream>

#include "diag/Timer.h"


struct NetworkResults {
	std::vector<uint16_t> data;

	NetworkResults(size_t size)
		: data(size) {
		
	}

	inline uint16_t& operator[](size_t i) {
		return data[i];
	}

        void printOut(const std::string& fileName) const {
		Timer t("printOut");

		std::ofstream f(fileName);
		if (!f.is_open()) {
			throw std::runtime_error("file for printing out results could not be opened");
		}

		for (uint16_t n : data) {
		        f << n << '\n';
		}
		f.close();
	}
};
