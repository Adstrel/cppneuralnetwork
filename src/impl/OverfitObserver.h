#pragma once

#include <iostream>
#include <vector>
#include "Matrix.h"


class OverfitObserver {
	float maximalDataAccuracy;
	std::vector<Matrix<float>> bestNetwork;
public:

	OverfitObserver(const std::vector<Matrix<float>>& originalNetwork) {
		maximalDataAccuracy = -1;
		bestNetwork = originalNetwork;
	}

	void newNetworkTest(const std::vector<Matrix<float>>& currentNetwork, float networksTestAccu) {
		if (networksTestAccu >= maximalDataAccuracy) {
			maximalDataAccuracy = networksTestAccu;
			bestNetwork = currentNetwork;
		}
	}

	const std::vector<Matrix<float>>& getBestNetwork() {
		return bestNetwork;
	}
};
