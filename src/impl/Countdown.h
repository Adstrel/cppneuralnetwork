#pragma once

#include <chrono>


class Countdown {
	std::chrono::time_point<std::chrono::steady_clock> countdownStart;
	std::chrono::seconds targetDuration;

public:
	template<typename Time_t>
	Countdown(const Time_t& _targetDuration) {
		countdownStart = std::chrono::steady_clock::now();
		targetDuration = std::chrono::duration_cast<std::chrono::seconds>(_targetDuration);
	};

	bool isTimeLeft() const {
		auto diff = std::chrono::duration_cast<std::chrono::seconds>(std::chrono::steady_clock::now() - countdownStart);

		return diff < targetDuration;
	}

	~Countdown() = default;
};
