#pragma once

#include <cmath>


inline float relu(float input) {
    return (input > 0.0f) * input;
}

inline float dRelu(float input) {
    return static_cast<float>(input > 0.0f);
}

/**
* Logistic sigmoid with 'lambda=1'
*/
inline float logisticSigmoid(float input) {
    return 1.0f / (1.0f + expf(-input));
}

/**
* Logistic sigmoid with 'lambda=1'
*/
inline float dLogisticSigmoid(float input) {
    float funcVal = logisticSigmoid(input);
    return funcVal * (1.0f - funcVal);
}


inline float meanSqError(float predicted, float expected) {
    float diff = predicted - expected;
    return diff * diff;
}


inline float crossEntropyError(float predicted, float expected) {
    return -expected * log(predicted + 1.e-8f);
}

// TODO This equation is wrong maybe? look into it!
/*inline float dCrossEntropyError(float predicted, float expected) {
    return -expected / predicted;
}*/

/*
 * Combined derivation of the Cross Entropy error function and Softmax activation
 * function.
 */
inline float dErrorCrossentropySoftmax(float predicted, float expected) {
    return predicted - expected;
}
