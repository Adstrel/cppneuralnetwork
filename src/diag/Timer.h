#pragma once

#include <chrono>
#include <iostream>
#include <string>


class Timer {
    std::string name;
    std::chrono::time_point<std::chrono::steady_clock> before;

public:
    Timer(const std::string& _name)
        : name(_name) {
        before = std::chrono::steady_clock::now();
    }

    ~Timer() {
        auto durationMicro = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::steady_clock::now() - before);

        std::cout << "Timer " << name << ": " << durationMicro.count() * 0.001f << " ms" << std::endl;
    }
};
