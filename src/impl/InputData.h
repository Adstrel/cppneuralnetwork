#pragma once

#include <array>
#include <vector>
#include <string>
#include <fstream>
#include <cstdlib>

#include "diag/Timer.h"


template<size_t SIZE>
struct InputData {
    std::array<float, SIZE> array;

    InputData(const std::string& line, float maxValue) {
        char* x = const_cast<char*>(line.c_str());
        array[0] = 1.0f;

        for (size_t i = 0; i < array.size(); i++) {
            array[i] = static_cast<float>(strtol(x, &x, 10)) / maxValue;
            x++;
        }
    }

    static std::vector<InputData<SIZE>> loadInputs(const std::string& pixelPath, float maxValue, size_t reserveSize = 16) {
        Timer t("loadPixels");

        std::ifstream f(pixelPath);
        if (!f.is_open()) {
            throw std::runtime_error("pixel file could not be opened");
        }

        std::vector<InputData<SIZE>> result;
        result.reserve(reserveSize);

        std::string line;
        while (std::getline(f, line)) {
            result.emplace_back(line, maxValue);
        }
        f.close();

        return result;
    }
};
