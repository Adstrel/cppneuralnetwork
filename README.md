# cppNeuralNetwork
Neural Network school project (FI MUNI).  
Initial assignment was to make a Neural Network without external libraries, that learns the Fashion MNIST dataset to at least 88 % test accuracy in 30 minutes on our school's server.  
Our final solution (commit ef0c9f8d) managed to achieve 89.34 % test accuracy.

## Authors
Adam Střelský, Jan Valenta
