#pragma once

#include <fstream>
#include <vector>
#include <array>
#include <string>
#include <cstring>

#include "diag/Timer.h"


template<size_t SIZE>
struct LabelData {
    std::array<float, SIZE> array;
    int resultIndex;


    LabelData(char c) {
        c -= '0';

        memset(array.data(), 0, sizeof(float) * array.size());
        array[c] = 1.0f;
        resultIndex = c;
    }

    static std::vector<LabelData<SIZE>> loadLabels(const std::string& labelPath, size_t reserveSize = 16) {
        Timer t("loadLabels");

        std::ifstream f(labelPath);
        if (!f.is_open()) {
            throw std::runtime_error("label file could not be opened");
        }

        std::vector<LabelData<SIZE>> result;
        result.reserve(reserveSize);

        std::string line;
        while (std::getline(f, line)) {
            result.emplace_back(line[0]);
        }
        f.close();

        return result;
    }
};
