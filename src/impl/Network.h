#pragma once

#include <cassert>

#include <iostream>
#include <vector>
#include <algorithm>
#include <cmath>
#include <cstdlib>

#include <omp.h>

#include "diag/Timer.h"

#include "Countdown.h"
#include "NetworkResults.h"
#include "Matrix.h"
#include "Maths.h"
#include "InputData.h"
#include "BatchSelector.h"
#include "OverfitObserver.h"


/**
 * Dense Neural Network
 *
 * Activation function of all hidden layers is ReLu
 * Activation function of the output layer is softmax
 */
template<size_t INPUT_SIZE, size_t OUTPUT_SIZE>
class Network {
    std::vector<Matrix<float>> layers;
    size_t neuronCount = 0; // without the input layer
    size_t lastLayerCount = 0;

    std::vector<unsigned int> layerSizes;

public:
    Network(const std::vector<unsigned int>& hiddenLayerSizes)
            : layerSizes(hiddenLayerSizes.size() + 2) {
        // Copy layer sizes
        layerSizes[0] = INPUT_SIZE;
        for (size_t i = 0; i < hiddenLayerSizes.size(); i++) {
            layerSizes[i + 1] = hiddenLayerSizes[i];
        }
        layerSizes[layerSizes.size() - 1] = OUTPUT_SIZE;

        // Initialize the layers and info about them
        layers.reserve(layerSizes.size() - 1);
        lastLayerCount = layerSizes.back();
        neuronCount = 0;
        for (size_t i = 1; i < layerSizes.size(); i++) {
            neuronCount += layerSizes[i];
            layers.emplace_back(layerSizes[i], layerSizes[i - 1] + 1);
            layers.back().resetRandom();
        }
    }

    template<typename Time_t>
    void train(const std::vector<InputData<INPUT_SIZE>>& pixelData, const std::vector<LabelData<OUTPUT_SIZE>>& labelData, Time_t trainTime,
        float initialStepSize, float momentumFactor, int batchSize, int testSetSize, float learnRateDecay, float noiseAddition, float weightDecayRate) {

        Timer t("trainTimer");
        Countdown cd(trainTime);

        // Batch training
        BatchSelector batchSelector(pixelData.size(), batchSize, testSetSize);

        // Early stopping
        OverfitObserver overfitObserver(layers);

        // Momentum
        std::vector<Matrix<float>> momentumMemory;
        momentumMemory.reserve(layerSizes.size() - 1);

        std::vector<float> neuronPotentials(neuronCount);
        std::vector<float> neuronResults(neuronCount);
        std::vector<float> errorOverNeuron(neuronCount);

        // Create 'weight change buffer' for remembering weight changes and "momentum buffer" for remembering last changes
        std::vector<Matrix<float>> weightChangeBuffer;
        weightChangeBuffer.reserve(layers.size());
        for (size_t i = 1; i < layerSizes.size(); i++) {
            weightChangeBuffer.emplace_back(layerSizes[i], layerSizes[i - 1] + 1);
            momentumMemory.emplace_back(layerSizes[i], layerSizes[i - 1] + 1);
        }
        for (auto& layer : momentumMemory) {
            layer.resetZero();
        }

        float validationSetAccuracy = 0.0f;
        int succesfulTrainingInputs = 0;
        size_t iter = 0;
        while (cd.isTimeLeft()) {
            std::cout << "\33[2K\rEpoch: " << iter << ", Training Accuracy: " << succesfulTrainingInputs / (float) batchSelector.getTrainingSize() << ", Validation Accuracy: " << validationSetAccuracy;
            std::cout.flush();

            succesfulTrainingInputs = 0;
            float realStepSize = 1 / (1 + learnRateDecay * iter) * initialStepSize;

            while (batchSelector.hasNext()) {
                
                for (auto& layer : weightChangeBuffer) {
                    layer.resetZero();
                }

                std::vector<size_t> indexArray = batchSelector.getNextBatch();

                //#pragma omp parallel for schedule(dynamic) private()
                for (size_t i = 0; i < indexArray.size(); i++) {
                    size_t exampleIndex = indexArray[i];

                    // Compute potentials and results for all neurons
                    evaluate(pixelData[exampleIndex], neuronPotentials, neuronResults, noiseAddition);

                    // Update this epoch's accuracy
                    succesfulTrainingInputs += isInputCorrect(neuronResults, labelData[exampleIndex]);

                    // Backpropagate and compute all values 'dError over dNeuronResult'
                    backpropagate(neuronResults, labelData[exampleIndex], errorOverNeuron, neuronPotentials);

                    // Compute 'dError over dWeight' and add it to 'weight change buffer'
                    computeWeightChange(neuronResults, pixelData[exampleIndex], errorOverNeuron, neuronPotentials, weightChangeBuffer);
                }

                // Update the network weights
                for (size_t layer = 0; layer < layers.size(); layer++) {
                    //Clone data from weight change matrix to momentum
                    for (size_t r = 0; r < layers[layer].rows; r++) {
                        for (size_t c = 0; c < layers[layer].cols; c++) {
                            float weightDelta = realStepSize * weightChangeBuffer[layer][r][c] / static_cast<float>(indexArray.size());
                            weightDelta += momentumFactor * momentumMemory[layer][r][c] + realStepSize * weightDecayRate * layers[layer][r][c];

                            layers[layer][r][c] -= weightDelta;
                            momentumMemory[layer][r][c] = weightDelta;
                        }
                    }
                }
            }
            batchSelector.goToStart();

            if (testSetSize > 0) {
                validationSetAccuracy = getSetAccuracy(batchSelector.getTestSet(), pixelData, labelData, neuronResults);
                overfitObserver.newNetworkTest(layers, validationSetAccuracy);
            }
            iter++;
        }
        if (testSetSize > 0) {
            layers = overfitObserver.getBestNetwork(); 
        }
        std::cout << std::endl;
    }

    float getSetAccuracy(const std::vector<size_t>& testIndicies, const std::vector<InputData<INPUT_SIZE>>& inputData,
        const std::vector<LabelData<OUTPUT_SIZE>>& labelData, std::vector<float>& neuronResults) {

        int correctClassification = 0;
        for (size_t index : testIndicies) {
            // Compute if this example is correct
            evaluate(inputData[index], neuronResults);
            correctClassification += isInputCorrect(neuronResults, labelData[index]);
        }
        float accuracy = static_cast<float>(correctClassification) / testIndicies.size();
        return accuracy;
    }

    bool isInputCorrect(const std::vector<float>& neuronResults, const LabelData<OUTPUT_SIZE>& label) {
        int64_t bestOutputIndex = -1;
        float bestOutputValue = -1;
        for (size_t j = 0; j < label.array.size(); j++) {
            if (neuronResults[neuronCount - lastLayerCount + j] > bestOutputValue) {
                bestOutputValue = neuronResults[neuronCount - lastLayerCount + j];
                bestOutputIndex = j;
            }
        }
        return (bestOutputIndex == label.resultIndex);
    }


    /**
    * Evaluate ALL examples
    */
    NetworkResults evaluate(const std::vector<InputData<INPUT_SIZE>>& pixelData) const {
        Timer t("evaluateAll");

        NetworkResults results(pixelData.size());

        if (pixelData.size() == 0) {
            return results;
        }


        #pragma omp parallel
        {
            std::vector<float> neuronResults(neuronCount);

            #pragma omp for schedule(dynamic)
            for (int64_t i = 0; i < static_cast<int64_t>(pixelData.size()); i++) {
                evaluate(pixelData[i], neuronResults);
                results[i] = static_cast<char>(std::distance(
                    neuronResults.data() + neuronCount - lastLayerCount,
                    std::max_element(
                        neuronResults.data() + neuronCount - lastLayerCount,
                        neuronResults.data() + neuronCount
                    )
                ));
            }
        }

        return results;
    }

private:
    void computeWeightChange(const std::vector<float>& neuronResults, const InputData<INPUT_SIZE>& input,
        const std::vector<float>& errorOverNeuron, const std::vector<float>& neuronPotentials,
        std::vector<Matrix<float>>& weightChangeBuffer) const {

        // Compute changes in weights connected in input neurons
        for (size_t j = 0; j < layerSizes[1]; j++) {
            Matrix<float>& emat = weightChangeBuffer[0];

            // Bias
            if (0 == layers.size() - 1) {
                /* Softmax */
                emat[j][0] += errorOverNeuron[j] *
                    /* derivation already computed in backpropagation */
                    1.0f;
            } else {
                emat[j][0] += errorOverNeuron[j] *
                    dRelu(neuronPotentials[j])  * 
                    1.0f ;
            }

            // Other weights
            for (size_t i = 0; i < layerSizes[0]; i++) {
                if (0 == layers.size() - 1) {
                    /* Softmax */
                    emat[j][i + 1] += errorOverNeuron[j] *
                        /* derivation already computed in backpropagation */
                        input.array[i];
                } else {
                    emat[j][i + 1] += errorOverNeuron[j] *
                        dRelu(neuronPotentials[j]) *
                        input.array[i];
                }
            }
        }

        // Compute changes in other weights
        size_t nextLayerBegin = 0;
        for (size_t layer = 1; layer < weightChangeBuffer.size(); layer++) {
            Matrix<float>& emat = weightChangeBuffer[layer];
            nextLayerBegin += layerSizes[layer];

            for (size_t j = 0; j < layerSizes[layer + 1]; j++) {
                // Bias
                if (layer == layers.size() - 1) {
                    /* Softmax */
                    emat[j][0] += errorOverNeuron[nextLayerBegin + j] *
                         /* derivation already computed in backpropagation */
                         1.0f ;
                } else {
                    emat[j][0] += errorOverNeuron[nextLayerBegin + j] *
                        dRelu(neuronPotentials[nextLayerBegin + j]) *
                        1.0f;
                }

                // Other weights
                for (size_t i = 0; i < layerSizes[layer]; i++) {
                    if (layer == layers.size() - 1) {
                        /* Softmax */
                        emat[j][i + 1] += errorOverNeuron[nextLayerBegin + j] *
                            /* derivation already computed in backpropagation */
                            neuronResults[nextLayerBegin + i - layerSizes[layer]];
                    } else {
                        emat[j][i + 1] += errorOverNeuron[nextLayerBegin + j] *
                            dRelu(neuronPotentials[nextLayerBegin + j]) *
                            neuronResults[nextLayerBegin + i - layerSizes[layer]];
                    }
                }
            }
        }
    }

    /**
    * Backpropagate ONE example
    */
    void backpropagate(const std::vector<float>& neuronResults, const LabelData<OUTPUT_SIZE>& expectedResult,
        std::vector<float>& errorOverNeuron, const std::vector<float>& neuronPotentials) const {

        // At the beginning, the input vector for the first neuron layer is the inputContainer
        size_t firstNeuronInLayer = neuronResults.size() - lastLayerCount;

        // Evaluate errors over output neurons
        for (size_t i = 0; i < lastLayerCount; i++) {
            errorOverNeuron[firstNeuronInLayer + i] = dErrorCrossentropySoftmax(neuronResults[firstNeuronInLayer + i], expectedResult.array[i]);
        }

        firstNeuronInLayer -= layerSizes[layerSizes.size() - 2];
        size_t firstNeuronInNextLayer = neuronResults.size() - lastLayerCount;

        // Evaluate errors over hidden neurons
        for (int64_t layer = layers.size() - 1; layer > 0; layer--) {
            const Matrix<float>& mat = layers[layer];

            for (size_t j = 0; j < mat.cols - 1; j++) {
                errorOverNeuron[firstNeuronInLayer + j] = 0.0f;
                for (size_t r = 0; r < mat.rows; r++) {
                    if (static_cast<unsigned>(layer) == layers.size() - 1) {
                        /* Softmax */
                        errorOverNeuron[firstNeuronInLayer + j] += errorOverNeuron[firstNeuronInNextLayer + r] *
                            /* softmax's derivation is combined with crossentropy's derivation */
                            mat[r][j + 1];
                    } else {
                        errorOverNeuron[firstNeuronInLayer + j] += errorOverNeuron[firstNeuronInNextLayer + r] *
                            dRelu(neuronPotentials[firstNeuronInNextLayer + r]) * mat[r][j + 1];
                    }
                }
            }

            // Move indicies
            firstNeuronInLayer -= layerSizes[layer - 1];
            firstNeuronInNextLayer -= layerSizes[layer];
        }
    }

    /**
    * Evaluate ONE example
    */
    // __declspec(noinline)
    void evaluate(const InputData<INPUT_SIZE>& inputData, std::vector<float>& neuronResults) const {
        // At the beginning, the input vector for the first neuron layer is the inputContainer
        const float* input = inputData.array.data();
        float* output = neuronResults.data();
        
        size_t layer = 0;
        for (auto& mat : layers) {
            // Evaluate 'outputVector = activationFunction(MAT * inputVector)'
            if (layer != layers.size() - 1) {
                for (size_t i = 0; i < mat.rows; i++) {
                    // Compute the potential
                    output[i] = mat[i][0];
                    for (size_t j = 1; j < mat.cols; j++) {
                        output[i] += input[j - 1] * mat[i][j];
                    }

                    // Apply the activation function
                    output[i] = relu(output[i]);
                }
            } else {
                float max = 0.0f;
                // Compute the potential
                for (size_t i = 0; i < mat.rows; i++) {
                    output[i] = mat[i][0];
                    for (size_t j = 1; j < mat.cols; j++) {
                        output[i] += input[j - 1] * mat[i][j];
                    }

                    if (i == 0 || max < output[i]) {
                        max = output[i];
                    }
                }
                
                // Apply the activation function - softmax
                float layerExpPotentialSum = 0.0f;
                for (size_t i = 0; i < mat.rows; i++) {
                    layerExpPotentialSum += exp(output[i] - max);
                }
                for (size_t i = 0; i < mat.rows; i++) {
                    output[i] = exp(output[i] - max) / (layerExpPotentialSum);
                }
            }

            // The current output vector is the next input vector
            input = output;
            // Move the outputvector by 'number of neurons in the last layer' values
            output += mat.rows;

            layer++;
        }
    }

    /**
    * Evaluate ONE example (used only for for training)
    * and save the computed potentials
    */
    // __declspec(noinline)
    void evaluate(const InputData<INPUT_SIZE>& inputData,
        std::vector<float>& neuronPotentials, std::vector<float>& neuronResults, float noiseAddition) const {
        // At the beginning, the input vector for the first neuron layer is the inputContainer
        const float* input = inputData.array.data();
        float* outputPotential = neuronPotentials.data();
        float* outputResult = neuronResults.data();

        std::array<float, INPUT_SIZE> inputNoise;
        for (float& val : inputNoise) {
            val = (static_cast<float>(rand()) / static_cast<float>(RAND_MAX)) * (noiseAddition * 2) - noiseAddition;
        }

        // Other neurons
        size_t layer = 0;
        for (auto& mat : layers) {
            // Evaluate 'outputVector = activationFunction(MAT * inputVector)'
            if (layer != layers.size() - 1) {
                for (size_t i = 0; i < mat.rows; i++) {
                    // Compute the potential
                    outputPotential[i] = mat[i][0];
                    for (size_t j = 1; j < mat.cols; j++) {
                        float noisyInput = input[j - 1];
                        if (layer == 0) {
                            noisyInput += inputNoise[j - 1];
                            noisyInput = std::max(noisyInput, 0.0f);
                            noisyInput = std::min(noisyInput, 1.0f);
                        }

                        outputPotential[i] += noisyInput * mat[i][j];
                    }

                    // Apply the activation function
                    outputResult[i] = relu(outputPotential[i]);
                }
            } else {
                float max = 0.0f;
                for (size_t i = 0; i < mat.rows; i++) {
                    // Compute the potential
                    outputPotential[i] = mat[i][0];
                    for (size_t j = 1; j < mat.cols; j++) {
                        float noisyInput = input[j - 1];
                        if (layer == 0) {
                            noisyInput += inputNoise[j - 1];
                            noisyInput = std::max(noisyInput, 0.0f);
                            noisyInput = std::min(noisyInput, 1.0f);
                        }

                        outputPotential[i] += noisyInput * mat[i][j];
                    }

                    if (i == 0 || max < outputPotential[i]) {
                        max = outputPotential[i];
                    }
                }

                // Apply the activation function - softmax
                float layerExpPotentialSum = 0.0f;
                for (size_t i = 0; i < mat.rows; i++) {
                    layerExpPotentialSum += exp(outputPotential[i] - max);
                }
                for (size_t i = 0; i < mat.rows; i++) {
                    outputResult[i] = exp(outputPotential[i] - max) / (layerExpPotentialSum);
                }
            }

            // The current output vector is the next input vector
            input = outputResult;
            // Move the outputvector by 'number of neurons in the last layer' values
            outputPotential += mat.rows;
            outputResult += mat.rows;

            layer++;
        }
    }
};

